# Instalacja środowiska ART-DECOR #


## Pobranie skryptów instalacyjnych ##

Stworzenie struktury katalogów:
```bash
$ sudo mkdir /opt/art-decor-linux
$ cd /opt/art-decor-linux
$ sudo mkdir tooling
$ sudo mkdir tooling/backups
$ sudo mkdir tooling/packages
$ sudo mkdir tooling/packages_archives
$ sudo mkdir tooling/scripts_archives
$ cd /opt/art-decor-linux/tooling
```

Instalacja narzędzia Subversion:
```bash
$ sudo apt install subversion
```

Pobranie skryptów instalacyjnych z repozytorium SVN ART-DECOR:
```bash
$ sudo svn checkout svn://svn.code.sf.net/p/artdecor/code-0/trunk/utilities/art_decor_installers/art-decor-linux/tooling/scripts scripts
```

## Instalacja środowiska Java ##

Instrukcja dla systemów linuksowych bazujących na dystrybucji Debian (np. Debian, Ubuntu,  Elementary, Mint) z wykorzystaniem managera pakietów APT.

Dodanie repozytorium APT z różnymi wersjami środowiska Java:
```bash
$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt update
```

Instalacja środowiska Java 8:
```bash
$ sudo apt install oracle-java8-set-default
```
Sprawdzenie poprawności wersji zainstalowanego środowiska Java
```bash
$ java -version
```
Przykładowy efekt wykonania polecenia:
```text
java version "1.8.0_171"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
```

## Instalacja serwera bazodanowego eXist-db ##

Pobranie serwera bazodanowego eXist-db w wersji 2.2 skonfigurowanej dla środowiska ART-DECOR:
```bash
$ cd /opt/art-decor-linux/tooling/packages 
$ sudo wget -O eXist-db-setup-2.2-rev0000.jar "https://downloads.sourceforge.net/project/artdecor/eXist-db/eXist-db-setup-2.2-rev0000.jar?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fartdecor%2Ffiles%2FeXist-db%2FeXist-db-setup-2.2-rev0000.jar%2Fdownload&ts=1527209634"
```

Zmiana formatu końca lini dla skrypu instalacyjnego:
```bash
$ cd /opt/art-decor-linux/tooling/scripts
$ sudo vi ./install_exist.sh
:set fileformat=unix
:wq
```

Uruchomianie skryptu instalacyjnego:
```bash
$ sudo chmod +x ./install_exist.sh
$ sudo ./install_exist.sh
```

Podczas wykonywania skryptu instalacyjnego pojawią się pytania o następujące parametry:
- ścieżka instalacji serwera eXist-db - należy pozostawić domyślną wskazaną przez skrypt, np. : `/usr/local/exist_atp_22_20151030`
- lokalizacja katalogu danych serwera - należy pozostawić domyślną `webapp/WEB-INF/data`
- **hasło administratora serwera**
- rozmiar maksymalnej pamięci do wykorzysatnia przez serwer w MB - pozostawić wartość domyślną
- rozmiar pamięci bufora w MB - pozostawić wartość domyślną

## Konfiguracja serwera bazodanowego eXist-db ##

### Wstęp ###
Optymalna konfiguracja pamięci wykorzystywanej przez serwer eXist-db:
- maksymalny rozmiar pamięci (maxMemory) - 8192 MB
- rozmiar bufora (cacheSize) - 2048MB - około 1/3 wartości rozmiaru pamięci maksymalnej
- rozmiar bufora dla kolekcji (collectionCache) - 2048MB - około 1/2 rozmiaru bufora

Ustalanie lokalizacji aktualnej instalacji środowiska Java:
```
$ update-alternatives --query java | grep 'Value: ' | grep -o '/.*/jre'
```

### Edycja pliku konfiguracyjnego wrapper.conf ###
```bash
$ sudo nano /usr/local/exist_atp/tools/wrapper/conf/wrapper.conf
```
Parametry konfiguracyjne:
- `wrapper.java.command` - lokalizacja komendy `java` zgodnie z aktualną instalacją środowiska Java
- `wrapper.java.maxmemory`- maksymalny rozmiar pamięci wykorzystywanej przez serwer

### Edycja pliku konfiguracyjnego eXist-settings.sh ###
```bash
$ sudo nano /usr/local/exist_atp/bin/functions.d/eXist-settings.sh
```

W funkcji `set_java_options` parametry `-Xms` oraz `-Xmx` powinny mieć taką samą wartość jak maksymalny rozmiar pamięci zdefiniowany w pliku configuracyjnym `wrapper.conf`.
Przykład:
```bash
set_java_options() {
  if [ -z "${JAVA_OPTIONS}" ]; then
    JAVA_OPTIONS="-Xms8192m -Xmx8192m -Dfile.encoding=UTF-8";
```

### Edycja pliku konfiguracyjnego conf.xml ###
```bash
$ sudo nano /usr/local/exist_atp/conf.xml
```

Parametry konfiguracyjne dla elementu `db-connection`:
- `cacheSize` - rozmiar pamięci dla bufora - około 1/3 wartości rozmiaru pamięci maksymalnej
- `collectionCache` - rozmiar pamięci dla bufora kolekcji - 1/2 wartości rozmiaru pamięci dla bufora

Przykład:
```xml
<db-connection cacheSize="2048M" collectionCache="1024M" database="native"
      files="webapp/WEB-INF/data" pageSize="4096" nodesBuffer="1000"
      doc-ids="default">
```

### Uruchomienie serwera eXist-db ###

Dodanie serwisu serwera bazodanowego eXist-db do skryptów startowych serwera:
```bash
$ sudo update-rc.d exist_atp defaults
```

Uruchomienie serwera:

```bash
$ sudo service exist_atp start
```

## Instalacja komponentów backend'owych środowiska ART-DECOR na bazie danych eXist-db ##

1. Uruchomienie panelu administracyjnego serwera eXist-db w przegladarce internetowej pod adresem `http://<adres-ip>:8877//apps/dashboard/index.html`.
2. Zalogowanie się przy pomocy hasła administratora poprzez kliknięcie w link w lewym górnym rogu aplikacji.
    - User: admin
    - Passowrd: hasło ustalone podczas instalacji serwera eXist-db

3. Uruchomienie aplikacji `Collections`.
4. Wybranie katalogu `apps`, a następnie `dashboard`.
5. Otworzenie do edycji pliku `configuration.xml` poprzez podwójne kliknięcie jego nazwy. Spowoduje to otworzenie nowego okna/zakładki przegladarki z edytorem eXide. 
6. Ustawienie zawartości elementu `<repository>` na adres repozytorium pakietów środowiska ART-DECOR:
```xml
<settings>
    <repository>http://decor.nictiz.nl/apps/public-repo</repository>
</settings>
```
7. Zapisanie zmodyfikowanego pliku `configuration.xml` poprzez kliknięcie w opcję `Save` w menu edytora oraz zamknięcie okna/zakładki przegladarki z edytorem eXide.
8. Uruchomienie aplikacji Package Manager.
9. Instalacja nowych pakietów w następującej kolejności:
    - Advanced Requirements Tooling
    - DECOR core files
    - DECOR services 
    - ART-DECOR System Services
    - Terminology Applications
    - OID Registry tools


## Instalacja serwera aplikacyjnego Tomcat ##

Instalacja serwera Tomcat 8:
```bash
$ sudo apt install tomcat8
```

Uruchomienie serwera Tomcat 8:
```bash
sudo service tomcat8 start
```

## Instalacja i konfiguracja aplikacji ART ##

Pobranie aplikacji ART:
```bash
$ cd /opt/art-decor-linux/tooling/packages 
$ sudo wget -O art-decor.war "https://downloads.sourceforge.net/project/artdecor/Orbeon/art-decor.war?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fartdecor%2Ffiles%2FOrbeon%2Fart-decor.war%2Fdownload&ts=1527217653"
```

Instalacja aplikacji ART:
```bash
sudo cp ./art-decor.war /var/lib/tomcat8/webapps
```